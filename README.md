# update-all-the-things [![build-status](https://pipelines-badges-service.useast.staging.atlassian.io/badge/atlassian/update-all-the-things.svg)](https://bitbucket.org/atlassian/update-all-the-things/addon/pipelines/home) [![Coverage Status](https://coveralls.io/repos/bitbucket/atlassian/update-all-the-things/badge.svg?branch=Coverage)](https://coveralls.io/bitbucket/atlassian/update-all-the-things?branch=Coverage)
![update all the things](http://treasure.diylol.com/uploads/post/image/267142/resized_all-the-things-meme-generator-update-all-the-things-ab9255.jpg)

## Installation

```sh
$ npm install --save update-all-the-things
```

## Usage

```js
var updateAllTheThings = require('update-all-the-things');

updateAllTheThings('Rainbow');
```
## License

Apache-2.0 © [Marco De Jongh]()


[npm-image]: https://badge.fury.io/js/update-all-the-things.svg
[npm-url]: https://npmjs.org/package/update-all-the-things
[travis-image]: https://travis-ci.org//update-all-the-things.svg?branch=master
[travis-url]: https://travis-ci.org//update-all-the-things
[daviddm-image]: https://david-dm.org//update-all-the-things.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//update-all-the-things
[coveralls-image]: https://coveralls.io/repos//update-all-the-things/badge.svg
[coveralls-url]: https://coveralls.io/r//update-all-the-things
